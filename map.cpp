﻿#include "map.h"
#include"config.h"
Map::Map()
{
    m_map1.load(GAME_IMG);
    m_map2.load(GAME_IMG);
    map1_y = -GAME_HEIGHT;
    map2_y = 0;
    map_speed = MAP_SPEED;
}
void Map::updatePosition(){
    map1_y += map_speed;
    if(map1_y >= 0){
        map1_y = -GAME_HEIGHT;
    }
    map2_y += map_speed;
    if(map2_y >= GAME_HEIGHT){
        map2_y = 0;
    }
}
