﻿#include "bullet.h"
#include"config.h"
Bullet::Bullet()
{
    bullet_pix.load(BULLET_IMG);
    b_x = 100;
    b_y = GAME_HEIGHT;
    is_free = true;
    bullet_speed = BULLET_SPEED;
    m_rect.setWidth(bullet_pix.width());
    m_rect.setHeight(bullet_pix.height());
    m_rect.moveTo(b_x,b_y);

}
void Bullet::updatePosition(){
    if(is_free){
        return;
    }
    b_y -= bullet_speed;
    if(b_y <= -bullet_pix.height()){
        is_free = true;
    }
    m_rect.moveTo(b_x,b_y);
}
