﻿#ifndef BULLET_H
#define BULLET_H

#include<QPixmap>
#include<QRect>
class Bullet
{
public:
    Bullet();
    QPixmap bullet_pix;
    int b_x;
    int b_y;
    int bullet_speed;
    bool is_free;
    QRect m_rect;
    void updatePosition();
};

#endif // BULLET_H
