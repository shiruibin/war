﻿#ifndef MAP_H
#define MAP_H
#include<QPixmap>

class Map
{
public:
    Map();
    QPixmap m_map1;
    QPixmap m_map2;
    int map1_y;
    int map2_y;
    int map_speed;
    void updatePosition();
};

#endif // MAP_H
