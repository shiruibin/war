﻿#ifndef CONFIG_H
#define CONFIG_H
#define GAME_TITLE "飞机大战"
#define GAME_WIDTH 512
#define GAME_HEIGHT 768
#define GAME_ICON ":/res/app.ico"
#define GAME_IMG ":/res/img_bg_level_3.jpg"
#define MAP_SPEED 2
#define TIMER_INT 10
#define HERO_IMG ":/res/hero.png"
#define HERO_SPEED 20
#define BULLET_IMG ":/res/bullet_8.png"
#define BULLET_SPEED 5
#define BULLET_NUM 30
#define BULLET_INT 30
#define ENEMY_IMG ":/res/img-plane_7.png"
#define ENEMY_SPEED 5
#define ENEMY_NUM 30
#define ENEMY_INT 30
#define BOMB_IMG ":/res/bomb-%1.png"
#define BOMB_MAX 7
#define BOMB_INT 10
#define BG_MUSIC ":/res/bg.wav"
#define BOMB_MUSIC ":/res/bomb.wav"
#endif // CONFIG_H
