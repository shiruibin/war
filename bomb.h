﻿#ifndef BOMB_H
#define BOMB_H
#include<QVector>
#include<QPixmap>
class Bomb
{
public:
    Bomb();
    QVector<QPixmap> bomb_imgs;
    int bomb_x;
    int bomb_y;
    bool is_free;
    int index;
    void changeImg();
    int bomb_int;
};

#endif // BOMB_H
