﻿#include "hero.h"
#include"config.h"
Hero::Hero()
{
    hero_pix.load(HERO_IMG);
    h_x = GAME_WIDTH *0.5 - hero_pix.width() *0.5;
    h_y = GAME_HEIGHT -hero_pix.height();
    hero_speed = HERO_SPEED;
    m_rect.setWidth(hero_pix.width());
    m_rect.setHeight(hero_pix.height());
    m_rect.moveTo(h_x,h_y);
    hero_int = 0;
}
void Hero::updatePosition(int x,int y){
    h_x = x;
    h_y = y;
    m_rect.moveTo(h_x,h_y);
}
void Hero::shoot(){
    hero_int++;
    if(hero_int <= BULLET_INT){
        return;
    }
    hero_int = 0;
    for(int i = 0;i<BULLET_NUM;i++){
        if(bullets[i].is_free){
            bullets[i].is_free = false;
            bullets[i].b_x = h_x + hero_pix.width()*0.5
                    - bullets[i].bullet_pix.width()*0.5;
            bullets[i].b_y = h_y;
            break;
        }
    }
}
