﻿#include "bomb.h"
#include"config.h"
Bomb::Bomb()
{
    for(int i=1;i<=BOMB_MAX;i++){
        QPixmap pix;
        //":/res/bomb-%1.png"
        pix.load(QString(BOMB_IMG).arg(i));
        bomb_imgs.push_back(pix);
    }
    bomb_x = 0;
    bomb_y = 0;
    is_free = true;
    index = 0;
    bomb_int =0;
}
void Bomb::changeImg(){
    if(is_free){
        return;
    }
    bomb_int++;
    if(bomb_int <= BOMB_INT){
        return;
    }
    bomb_int =0;
    index++;
    if(index >6){
        is_free = true;
        index = 0;
    }
}
