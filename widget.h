﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "map.h"
#include<QPaintEvent>
#include<QTimer>
#include<hero.h>
#include<QKeyEvent>
//#include<bullet.h>
#include<enemy.h>
#include<bomb.h>
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void initScene();
    Map m_map;
    Hero m_hero;
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    QTimer m_timer;
    void playGame();
    void updatePosition();
    Enemy enemies[ENEMY_NUM];
    void enemyToScene();
    int enemy_int;
    void collisionDetection();
    Bomb bombs[ENEMY_NUM];
private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
