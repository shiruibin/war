﻿#include "enemy.h"
#include"config.h"
Enemy::Enemy()
{
    enemy_pix.load(ENEMY_IMG);
    e_x = 0;
    e_y = 0;
    enemy_speed = ENEMY_SPEED;
    is_free = true;
    m_rect.setWidth(enemy_pix.width());
    m_rect.setHeight(enemy_pix.height());
    m_rect.moveTo(e_x,e_y);
}
void Enemy::updatePosition(){
    if(is_free){
        return;
    }
    e_y += enemy_speed;
    if(e_y >=GAME_HEIGHT){
        is_free =true;
    }
    m_rect.moveTo(e_x,e_y);
}
