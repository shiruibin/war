﻿#include "widget.h"
#include "ui_widget.h"
#include<QIcon>
#include"config.h"
#include<QPainter>
#include<ctime>
#include<QSound>
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    initScene();
    srand(unsigned(time(NULL)));
    enemy_int = 0;
}
void Widget::initScene(){
    setWindowTitle(GAME_TITLE);
    setFixedSize(GAME_WIDTH,GAME_HEIGHT);
    setWindowIcon(QIcon(GAME_ICON));
    m_timer.setInterval(TIMER_INT);
    playGame();
}
void Widget::playGame(){
    m_timer.start();
    connect(&m_timer,&QTimer::timeout,[=](){
        collisionDetection();
        updatePosition();
        update();
    });
}
void Widget::updatePosition(){
    m_map.updatePosition();
    enemyToScene();
    for(int i=0;i<ENEMY_NUM;i++){
        if(!enemies[i].is_free){
            enemies[i].updatePosition();
        }
    }
    m_hero.shoot();
    for(int i = 0;i<BULLET_NUM;i++){
        if(!m_hero.bullets[i].is_free){
            m_hero.bullets[i].updatePosition();
        }
    }
    for(int i =0;i<ENEMY_NUM;i++){
        if(!bombs[i].is_free){
            bombs[i].changeImg();
        }
    }
}
void Widget::keyPressEvent(QKeyEvent *event){
    int x = m_hero.h_x;
    int y = m_hero.h_y;
    if(event->key() == Qt::Key_Up){
        y -= m_hero.hero_speed;
    }
    if(y <= 0){
        y = 0;
    }
    if(event->key() == Qt::Key_Down){
        y += m_hero.hero_speed;
    }
    if(y >= GAME_HEIGHT - m_hero.hero_pix.height()){
        y = GAME_HEIGHT - m_hero.hero_pix.height();
    }

    if(event->key() == Qt::Key_Left){
        x -= m_hero.hero_speed;
    }
    if(x <= 0){
        x = 0;
    }
    if(event->key() == Qt::Key_Right){
        x += m_hero.hero_speed;
    }
    if(x>= GAME_WIDTH - m_hero.hero_pix.width()){
        x = GAME_WIDTH-m_hero.hero_pix.width();
    }
    m_hero.updatePosition(x,y);


}

void Widget::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    painter.drawPixmap(0,m_map.map1_y,m_map.m_map1);
    painter.drawPixmap(0,m_map.map2_y,m_map.m_map2);
    painter.drawPixmap(m_hero.h_x,m_hero.h_y,m_hero.hero_pix);

    for(int i = 0;i<BULLET_NUM;i++){
        if(!m_hero.bullets[i].is_free){
            painter.drawPixmap(m_hero.bullets[i].b_x,m_hero.bullets[i].b_y,m_hero.bullets[i].bullet_pix);
        }
    }

    for(int i=0;i<ENEMY_NUM;i++){
        if(!enemies[i].is_free){
            painter.drawPixmap(enemies[i].e_x,enemies[i].e_y,enemies[i].enemy_pix);
        }
    }
    for(int i =0;i<ENEMY_NUM;i++){
        if(!bombs[i].is_free){
            painter.drawPixmap(bombs[i].bomb_x,bombs[i].bomb_y,bombs[i].bomb_imgs[bombs[i].index]);
        }
    }
}
void Widget::enemyToScene(){
    enemy_int++;
    if(enemy_int <= ENEMY_INT){
        return;
    }
    enemy_int = 0;
    for(int i = 0;i<ENEMY_NUM;i++){
        if(enemies[i].is_free){
            enemies[i].is_free = false;
            enemies[i].e_y = -enemies[i].enemy_pix.height();
            //srand  rand()
            enemies[i].e_x = rand()%(GAME_WIDTH - enemies[i].enemy_pix.width());
            break;
        }
    }
}
void Widget::collisionDetection(){
    for(int i =0;i<BULLET_NUM;i++){
        if(m_hero.bullets[i].is_free){
            continue;
        }
        for(int j = 0;j<ENEMY_NUM;j++){
            if(enemies[j].is_free){
                continue;
            }
            if(m_hero.bullets[i].m_rect.intersects(enemies[j].m_rect)){
                m_hero.bullets[i].is_free =true;
                enemies[j].is_free = true;
                QSound::play(BOMB_MUSIC);
                for(int k =0;k <ENEMY_NUM;k++){
                    if(bombs[k].is_free){
                        bombs[k].is_free = false;
                        bombs[k].bomb_x = enemies[j].e_x;
                        bombs[k].bomb_y = enemies[j].e_y;
                        break;
                    }
                }
            }
        }
    }
}

Widget::~Widget()
{
    delete ui;
}
