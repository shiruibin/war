﻿#ifndef HERO_H
#define HERO_H

#include<QPixmap>
#include<QRect>
#include<bullet.h>
#include"config.h"
class Hero
{
public:
    Hero();
    QPixmap hero_pix;
    int h_x;
    int h_y;
    QRect m_rect;
    int hero_speed;
    void updatePosition(int x,int y);
    void shoot();
    Bullet bullets[BULLET_NUM];
    int hero_int;
};

#endif // HERO_H
