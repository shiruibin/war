﻿#ifndef ENEMY_H
#define ENEMY_H
#include<QPixmap>
#include<QRect>
class Enemy
{
public:
    Enemy();
    QPixmap enemy_pix;
    int e_x;
    int e_y;
    int enemy_speed;
    bool is_free;
    QRect m_rect;
    void updatePosition();
};

#endif // ENEMY_H
